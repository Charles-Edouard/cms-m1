<?php
    session_start();

    require_once 'vendor/autoload.php';

    use App\Controller\HomeController;

    // Micro routing
    if (isset($_GET["url"])) {
        $className = ucwords($_GET["url"]);

        $class = "App\\Controller\\". $className ."Controller";
        $objet = new $class();

        $method = $_GET["method"];

        if (isset($_GET["id"])) {
            $objet->$method($_GET["id"]);
        }
        else {
            $objet->$method();
        }
    } 
    else {
        $home = new HomeController();

        $home->showAllArticle();
    }
?>