<?php

namespace App\Entity;

use App\Manager\ArticleManager;

class Article
{
    // Propriétés ou Attributs
    private $idArticle,
            $title,
            $content,
            $imgPath,
            $author,
            $createdDate,
            $updatedDate;

    // Accesseurs ou Getters & Setters
    /**
     * Get the value of idArticle
     */ 
    public function getIdArticle()
    {
        return $this->idArticle;
    }

    /**
     * Set the value of idArticle
     *
     * @return  self
     */ 
    public function setIdArticle($idArticle)
    {
        $this->idArticle = $idArticle;

        return $this;
    }

    /**
     * Get the value of title
     */ 
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set the value of title
     *
     * @return  self
     */ 
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get the value of content
     */ 
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set the value of content
     *
     * @return  self
     */ 
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get the value of imgPath
     */ 
    public function getImgPath()
    {
        return $this->imgPath;
    }

    /**
     * Set the value of imgPath
     *
     * @return  self
     */ 
    public function setImgPath($imgPath)
    {
        $this->imgPath = $imgPath;

        return $this;
    }

    /**
     * Get the value of author
     */ 
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set the value of author
     *
     * @return  self
     */ 
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get the value of createdDate
     */ 
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * Set the value of createdDate
     *
     * @return  self
     */ 
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;

        return $this;
    }

    /**
     * Get the value of updatedDate
     */ 
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * Set the value of updatedDate
     *
     * @return  self
     */ 
    public function setUpdatedDate($updatedDate)
    {
        $this->updatedDate = $updatedDate;

        return $this;
    }

    // Constructeur
    public function __construct(array $array = [])
    {
        if (!empty($array)) {
            $this->setIdArticle($array["id_article"]);
            $this->setTitle($array["title"]);
            $this->setContent($array["content"]);
            $this->setImgPath($array["img_path"]);
            $this->setAuthor($array["author"]);
            $this->setCreatedDate($array["created_date"]);
            $this->setUpdatedDate($array["updated_date"]);
        }
    }

    // Function to get author name
    public function findAuthorName($author)
    {
        $authorName = ArticleManager::findAuthorNameArticle($author);

        return $authorName;
    }
}