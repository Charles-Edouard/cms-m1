<?php

namespace App\Controller;

use App\Manager\ArticleManager;

class ArticleController
{
    // public function showAllArticle()
    // {
    //     $listArticle = ArticleManager::getAllArticle();

    //     include 'template/home.php';
    // }

    public function showOneArticle($id)
    {
        $article = ArticleManager::getOneArticle($id);
        $title = $article->getTitle();

        include 'template/article.php';
    }
}