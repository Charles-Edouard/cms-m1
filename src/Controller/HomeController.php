<?php
namespace App\Controller;

use App\Manager\ArticleManager;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


class HomeController
{
    public function showAllArticle()
    {
        $listArticle = ArticleManager::getAllArticle();
        $title = "Bienvenu sur mon site";

        include 'template/home.php';
    }

    public function showContactHome() 
    {
        $title = "Contact";

        include 'template/contact.php';
    }
    
}