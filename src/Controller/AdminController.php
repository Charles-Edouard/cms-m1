<?php

namespace App\Controller;

use App\Manager\ArticleManager;
use App\Entity\Article;
use App\Entity\User;
use App\Manager\UserManager;

class AdminController
{
    public function showAdmin()
    {
        if (isset($_SESSION["id_user"]) && $_SESSION["id_user"] === 1) {
            $listArticle = ArticleManager::getAllArticle();
    
            include 'template/admin.php';
        } 
        else {
            include 'template/login.php';
        }
    }
    public function showAbout()
    {
        include 'template/about.php';
    }


    // public function login()
    // {
    //     $this->showAdmin();
    // }

    public function logout()
    {
        UserManager::logout();
        header('Location: /charles_edouard_website');
    }

    public function formLogin()
    {
        if (isset($_POST["btnConnection"]) && !is_null($_POST["btnConnection"])) {
            $u = new User();
            $u->setUsername($_POST["username"]);
            $u->setPassword($_POST["password"]);

            UserManager::uLogin($u);
        }

        if (isset($_SESSION["id_user"]) && $_SESSION["id_user"] === 1) {
            $this->showAdmin();
        } 
        else {
            include 'template/login.php';
        }
    }

    public function showFormArticle()
    {
        $listUser = UserManager::getAllUser();

        include 'template/form_article.php';
    }

    public function showFormEditArticle()
    {
        $article = ArticleManager::getOneArticle($_GET['id']);
        $id_article = $_GET['id'];
        $listUser = UserManager::getAllUser();

        include 'template/form_editArticle.php';
    }

    public function showFormDeleteArticle()
    {
        $id_article = $_GET['id'];

        include 'template/form_deleteArticle.php';
    }

    public function addArticle()
    {
        $article = new Article();
        $article->setTitle($_POST["title"]);
        $article->setContent($_POST["content"]);
        $article->setAuthor($_POST["author"]);
        $article->setImgPath($_POST["img"]);
        $article->setCreatedDate(date("Y-m-d"));
        $article->setUpdatedDate(date("Y-m-d"));

        ArticleManager::insertArticle($article);

        $this->showAdmin();
    }

    public function editArticle()
    {
        $article = new Article();
        $article->setIdArticle($_POST["id_article"]);
        $article->setTitle($_POST["title"]);
        $article->setContent($_POST["content"]);
        $article->setAuthor($_POST["author"]);
        $article->setImgPath($_POST["img"]);
        $article->setUpdatedDate(date("Y-m-d"));

        ArticleManager::updateArticle($article);

        $this->showAdmin();
    }

    public function deleteArticle()
    {
        ArticleManager::deleteArticle($_POST['id_article']);

        $this->showAdmin();
    }
}