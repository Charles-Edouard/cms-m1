<?php

namespace App\Manager;

use PDO;
use PDOException;
use App\Manager\Manager;
use App\Entity\Article;

class ArticleManager extends Manager
{
    public static function getAllArticle()
    {
        $sql = "SELECT * FROM article";
        $listArticle = [];

        // Ouverture de la connexion à la BDD
        $co = Manager::dbConnection();

        try {
            $stmt = $co->prepare($sql);
            $stmt->execute();

            while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $listArticle[] = new Article($data);
            }
        }
        catch(PDOException $e) {
            return $e->getCode() ." : ". $e->getMessage();
        }

        // Fermeture de la connexion à la BDD
        Manager::dbDisconnection($co);

        return $listArticle;
    }

    public static function getOneArticle($id)
    {
        $sql = "SELECT * FROM article WHERE id_article=:id_article";

        // Ouverture de la connexion à la BDD
        $co = Manager::dbConnection();

        try {
            $stmt = $co->prepare($sql);
            $stmt->bindParam(":id_article", $id);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e) {
            return $e->getCode() ." : ". $e->getMessage();
        }

        // Fermeture de la connexion à la BDD
        Manager::dbDisconnection($co);

        return new Article($data);
    }

    public static function insertArticle(Object $article)
    {
        $sql = "INSERT INTO article (title, content, img_path, author, created_date, updated_date)
                VALUES (:title, :content, :img_path, :author, :created_date, :updated_date)";

        $title = $article->getTitle();
        $content = $article->getContent();
        $imgPath = $article->getImgPath();
        $author = $article->getAuthor();
        $createdDate = $article->getCreatedDate();
        $updatedDate = $article->getUpdatedDate();

        // Ouverture de la connexion à la BDD
        $co = Manager::dbConnection();

        try {
            $stmt = $co->prepare($sql);
            $stmt->bindParam(":title", $title);
            $stmt->bindParam(":content", $content);
            $stmt->bindParam(":img_path", $imgPath);
            $stmt->bindParam(":author", $author);
            $stmt->bindParam(":created_date", $createdDate);
            $stmt->bindParam(":updated_date", $updatedDate);
            $stmt->execute();
        }
        catch(PDOException $e) {
            return $e->getCode() ." : ". $e->getMessage();
        }

        // Fermeture de la connexion à la BDD
        Manager::dbDisconnection($co);
    }

    public static function updateArticle(Object $article)
    {
        $sql = "UPDATE article
                SET title=:title, content=:content, img_path=:img_path, author=:author, updated_date=:updated_date
                WHERE id_article=:id_article";

        $id_article = $article->getIdArticle();
        $title = $article->getTitle();
        $content = $article->getContent();
        $imgPath = $article->getImgPath();
        $author = $article->getAuthor();
        $updatedDate = $article->getUpdatedDate();

        // Ouverture de la connexion à la BDD
        $co = Manager::dbConnection();

        try {
            $stmt = $co->prepare($sql);
            $stmt->bindParam(":id_article", $id_article);
            $stmt->bindParam(":title", $title);
            $stmt->bindParam(":content", $content);
            $stmt->bindParam(":img_path", $imgPath);
            $stmt->bindParam(":author", $author);
            $stmt->bindParam(":updated_date", $updatedDate);
            $stmt->execute();
        }
        catch(PDOException $e) {
            return $e->getCode() ." : ". $e->getMessage();
        }

        // Fermeture de la connexion à la BDD
        Manager::dbDisconnection($co);
    }

    public static function deleteArticle($id)
    {
        $sql = "DELETE FROM article WHERE id_article=:id_article";

        // Ouverture de la connexion à la BDD
        $co = Manager::dbConnection();

        try {
            $stmt = $co->prepare($sql);
            $stmt->bindParam(':id_article', $id);
            $stmt->execute();
        }
        catch(PDOException $e) {
            echo $e->getCode() ." : ". $e->getMessage();
        }

        // Fermeture de la connexion à la BDD
        Manager::dbDisconnection($co);
    }

    public static function findAuthorNameArticle($author)
    {
        $sql = "SELECT * FROM user WHERE id_user=:author";

        // Ouverture de la connexion à la BDD
        $co = Manager::dbConnection();

        try {
            $stmt = $co->prepare($sql);
            $stmt->bindParam(":author", $author);
            $stmt->execute();

            $data = $stmt->fetch(PDO::FETCH_ASSOC);
        }
        catch(PDOException $e) {
            return $e->getCode() ." : ". $e->getMessage();
        }

        // Fermeture de la connexion à la BDD
        Manager::dbDisconnection($co);

        return $data["username"];
    }
}