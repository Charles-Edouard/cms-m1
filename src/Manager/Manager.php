<?php

namespace App\Manager;

use PDO;
use PDOException;

class Manager
{
    const SERVER_NAME = "localhost";
    const DATA_BASE = "master1820_article-poo";
    const USER_NAME = "root";
    const PASSWORD = "root";

    protected static function dbConnection()
    {
        try {
            $co = new PDO(
                "mysql:host=". self::SERVER_NAME .";dbname=". self::DATA_BASE,
                self::USER_NAME,
                self::PASSWORD
            );

            $co->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // echo 'Connexion réussi';
        }
        catch (PDOException $e) {
            $e->getMessage();
        }

        return $co;
    }

    protected static function dbDisconnection($co)
    {
        $co = null;
    }
}