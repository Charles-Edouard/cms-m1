<?php

namespace App\Manager;

use App\Manager\Manager;
use App\Entity\User;
use PDO;
use PDOException;

class UserManager extends Manager
{
    public static function uLogin(User $user)
    {
        $sql = "SELECT * FROM user WHERE username=:username";
        $username = $user->getUsername();

        try {
            $co = parent::dbConnection();

            $stmt = $co->prepare($sql);

            $stmt->bindParam(':username', $username);

            $stmt->execute();

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($stmt->rowCount() > 0) {
                if (password_verify($user->getPassword(), $row["password"])) {
                    $_SESSION["id_user"] = intval($row["id_user"]);
                }
                else {
                    $_SESSION['id_user'] = 0;
                }
            }
            else {
                $_SESSION['id_user'] = 0;
            }

            parent::dbDisconnection($co);
        } 
        catch (PDOException $e) {
            $e->getMessage();
        }
    }

    public static function logout()
    {
        session_destroy();
        unset($_SESSION["id_user"]);
    }

    public static function getAllUser()
    {
        $sql = "SELECT * FROM user";
        $listUser = [];

        // Ouverture de la connexion à la BDD
        $co = Manager::dbConnection();

        try {
            $stmt = $co->prepare($sql);
            $stmt->execute();

            while ($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $listUser[] = new User($data);
            }
        }
        catch(PDOException $e) {
            return $e->getCode() ." : ". $e->getMessage();
        }

        // Fermeture de la connexion à la BDD
        Manager::dbDisconnection($co);

        return $listUser;
    }
}