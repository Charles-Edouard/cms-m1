<?php include 'layout/head_admin.php' ?>
<?php include 'layout/menu_admin.php' ?>

<h2>Modifier un article</h2>

<form action="?url=admin&method=editArticle" method="post">
    <div class="form-group">
        <label for="title">Titre de l'article</label>
        <input type="text" class="form-control" name="title" id="title" value="<?=$article->getTitle() ?>">
    </div>

    <div class="form-group">
        <label for="content">Contenu de l'article</label>
        <textarea name="content" class="form-control" id="content" cols="30" rows="10"><?=$article->getContent() ?></textarea>
    </div>

    <div class="form-group">
        <label for="img">Image de l'article</label>
        <input type="text" class="form-control" name="img" id="img" value="<?=$article->getImgPath() ?>">
    </div>

    <div class="form-group">
        <label for="author">Nom de l'auteur</label>
        <select name="author" id="author" class="form-control">
            <option value="0">Choisir un auteur</option>
            <?php foreach ($listUser as $user) { ?>
                <option value="<?= $user->getIdUser() ?>"
                    <?= $selected = ($user->getIdUser() == $article->getAuthor()) ? "selected" : "" ?>
                ><?= $user->getUsername() ?></option>
            <?php } ?>
        </select>
    </div>

    <input type="hidden" name="id_article" value="<?=$id_article ?>">
    <input type="submit" class="btn btn-warning" value="Modifier">
</form>

<?php include 'layout/footer_admin.php' ?>