<?php include 'layout/head.php' ?>
<?php include 'layout/menu.php' ?>


<h1><?= $article->getTitle() ?></h1>

<div>
    <span><?= $article->getAuthor() ?></span>
    <span><?= $article->getUpdatedDate() ?></span>
</div>

<p>
    <?= $article->getContent() ?>
</p>


<?php include 'layout/footer.php' ?>