<?php include 'layout/head.php' ?>
<?php include 'layout/menu.php' ?>

<div class=" uk-child-width-1-2@s uk-grid-match" uk-grid>
    <?php foreach ($listArticle as $article) { ?>
        <div>
            <div class="uk-card uk-card-hover uk-card-body">
                <h3 class="uk-card-title"><?= $article->getTitle() ?></h3>
                <h5 class="card-title"><time datetime="2016-04-01T19:00"><?= $article->getUpdatedDate() ?></time></h5>
                <p><?= $article->getContent() ?></p>
                <a href="?url=article&method=showOneArticle&id=<?= $article->getIdArticle() ?>" class="btn btn-primary">Voir Plus</a>
            </div>
        </div>
    <?php } ?>
</div>

<div class="uk-section uk-section-primary uk-light">
    <div class="uk-container">

        <h3>Section Primary</h3>

        <div class="uk-grid-match uk-child-width-1-3@m" uk-grid>
            <div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
            </div>
            <div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
            </div>
            <div>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
            </div>
        </div>

    </div>
</div>
<?php include 'layout/footer.php' ?>
