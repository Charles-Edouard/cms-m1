<?php include 'layout/head_admin.php' ?>
<?php include 'layout/menu_admin.php' ?>

<h1>Administration</h1>

<div>
    <a href="?url=admin&method=showFormArticle" class="btn btn-success">Ajouter un Article</a>
</div>

<table class="table table-striped mt-4">
    <thead class="thead-dark">
        <tr>
            <th>Titre</th>
            <th>Auteur</th>
            <th>Date de création</th>
            <th>Date de modification</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($listArticle as $article) { ?>
        <tr>
            <td><?= $article->getTitle() ?></td>
            <td><?= $article->findAuthorName($article->getAuthor()) ?></td>
            <td><?= $article->getCreatedDate() ?></td>
            <td><?= $article->getUpdatedDate() ?></td>
            <td>
                <a uk-icon="icon: file-edit" href="?url=admin&method=showFormEditArticle&id=<?= $article->getIdArticle() ?>">Editer</a>
                <a uk-icon="icon: trash"  href="?url=admin&method=showFormDeleteArticle&id=<?= $article->getIdArticle() ?>">Supprimer</a>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>

<?php include 'layout/footer_admin.php' ?>
