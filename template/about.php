<?php include 'layout/head.php' ?>
<?php include 'layout/menu.php' ?>
<div class="uk-cover-container uk-height-medium uk-margin-large-bottom">
    <video src="https://yootheme.com/site/images/media/yootheme-pro.mp4" autoplay loop muted playsinline uk-cover></video>
</div>
<button class="uk-button uk-button-danger uk-button-large uk-position-center uk-margin-large" type="button" uk-toggle="target: #modal-example">Me connaître davantage</button>

<!-- This is the modal -->
<div id="modal-example" uk-modal>
    <div class="uk-modal-dialog uk-modal-body">
        <h2 class="uk-modal-title">Headline</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        <p class="uk-text-right">
            <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>        </p>
    </div>
</div>

<div class="uk-section uk-section-primary uk-preserve-color uk-margin-large-top">
    <div class="uk-container">

        <div class="uk-panel uk-light uk-margin-medium">
            <h3>Section Primary with cards</h3>
        </div>

        <div class="uk-grid-match uk-child-width-expand@m" uk-grid>
            <div>
                <div class="uk-card uk-card-default uk-card-body">
                    <li><a href="?url=home&method=showAllArticle">Voir le blog</a></li>
                </div>
            </div>
            <div>
                <div class="uk-card uk-card-default uk-card-body">
                    <li><a href="?url=home&method=showContactHome">Contactez moi</a></li>
                </div>
            </div>
        </div>

    </div>
</div>
<?php include 'layout/footer.php' ?>
