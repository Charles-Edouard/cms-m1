<?php include 'layout/head_admin.php' ?>
<?php include 'layout/menu_admin.php' ?>

<h2>Ajouter un article</h2>

<form action="?url=admin&method=addArticle" method="post">
    <div class="form-group">
        <label for="title">Titre de l'article</label>
        <input type="text" class="form-control" name="title" id="title">
    </div>

    <div class="form-group">
        <label for="content">Contenu de l'article</label>
        <textarea name="content" class="form-control" id="content" cols="30" rows="10"></textarea>
    </div>

    <div class="form-group d-flex">
        <div class="w-75">
            <label for="img">Image de l'article</label>
            <input type="hidden" name="img" id="img-name" value="">
            <input type="file" class="form-control" id="img">
        </div>
        <div class="w-25 text-right">
            <img src="https://picsum.photos/70" height="70px" id="display-img">
        </div>
    </div>

    <script>
        const fileInputImg = document.getElementById("img");

        fileInputImg.addEventListener('change', () => {

            const form = new FormData();
            const file = fileInputImg.files[0];

            form.append('upfile', file);

            const url = "vendor/upload/upload.php";

            const request = new Request(url, {
                method: 'POST',
                body: form
            });

            fetch(request)
                .then(response => response.json().then(res => {
                    if (!res.error) {
                        document.getElementById("img-name").value = res.filename
                        document.getElementById("display-img").setAttribute("src", "public/images/" + res.filename)
                    } else {
                        console.log(res.message);
                    }
                }))
        })
    </script>

    <div class="form-group">
        <label for="author">Nom de l'auteur</label>
        <select name="author" id="author" class="form-control">
            <option value="0">Choisir un auteur</option>
            <?php foreach ($listUser as $user) { ?>
                <option value="<?= $user->getIdUser() ?>"><?= $user->getUsername() ?></option>
            <?php } ?>
        </select>
    </div>

    <input type="submit" class="btn btn-success" value="Ajouter">
</form>

<?php include 'layout/footer_admin.php' ?>