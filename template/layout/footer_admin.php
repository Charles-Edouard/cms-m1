    </div>
    
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
    <script type="text/javascript"
        src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>

    <script type="text/javascript" src="public/js/froala_editor.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/align.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/char_counter.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/code_beautifier.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/code_view.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/colors.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/draggable.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/emoticons.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/entities.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/file.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/font_size.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/font_family.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/fullscreen.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/image.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/image_manager.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/line_breaker.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/inline_style.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/link.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/lists.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/paragraph_format.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/paragraph_style.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/quick_insert.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/quote.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/table.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/save.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/url.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/video.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/help.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/print.min.js"></script>
    <!-- <script type="text/javascript" src="public/js/third_party/spell_checker.min.js"></script> -->
    <script type="text/javascript" src="public/js/plugins/special_characters.min.js"></script>
    <script type="text/javascript" src="public/js/plugins/word_paste.min.js"></script>

    <script>
        (function () {
            new FroalaEditor("#content")
        })()
    </script>

</body>
</html>