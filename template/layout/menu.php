<div class="uk-position-relative uk-margin-xlarge">
    <div class="uk-position-top">
        <nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
            <div class="uk-navbar-left">
                <ul class="uk-navbar-nav">
                    <li class="uk-active"><a href="?url=home&method=showAllArticle">Accueil</a></li>
                    <li>
                        <a href="#">Menu</a>
                        <div class="uk-navbar-dropdown">
                            <ul class="uk-nav uk-navbar-dropdown-nav">
                                <li><a href="?url=admin&method=showAbout">A Propos</a></li>
                                <li><a href="?url=home&method=showContactHome   ">Contactez moi</a></li>
                                <li><a href="?url=admin&method=showAdmin">Admin</a></li>
                                <li class="uk-nav-divider"></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>


