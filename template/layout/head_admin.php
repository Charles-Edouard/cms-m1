<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Administration</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="public/css/froala_editor.css">
    <link rel="stylesheet" href="public/css/froala_style.css">
    <link rel="stylesheet" href="public/css/plugins/code_view.css">
    <link rel="stylesheet" href="public/css/plugins/draggable.css">
    <link rel="stylesheet" href="public/css/plugins/colors.css">
    <link rel="stylesheet" href="public/css/plugins/emoticons.css">
    <link rel="stylesheet" href="public/css/plugins/image_manager.css">
    <link rel="stylesheet" href="public/css/plugins/image.css">
    <link rel="stylesheet" href="public/css/plugins/line_breaker.css">
    <link rel="stylesheet" href="public/css/plugins/table.css">
    <link rel="stylesheet" href="public/css/plugins/char_counter.css">
    <link rel="stylesheet" href="public/css/plugins/video.css">
    <link rel="stylesheet" href="public/css/plugins/fullscreen.css">
    <link rel="stylesheet" href="public/css/plugins/file.css">
    <link rel="stylesheet" href="public/css/plugins/quick_insert.css">
    <link rel="stylesheet" href="public/css/plugins/help.css">
    <!-- <link rel="stylesheet" href="public/css/third_party/spell_checker.css"> -->
    <link rel="stylesheet" href="public/css/plugins/special_characters.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
</head>
<body>
    <div class="container-fluid">