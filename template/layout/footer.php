<footer class="footer-distributed">

			<div class="footer-left">

				<h3>Company<span>logo</span></h3>

				<p class="footer-links">
					<li><a href="?url=admin&method=showAbout">A Propos</a></li>
                	<li><a href="?url=home&method=showContactHome">Contactez moi</a></li>
                	<li><a href="?url=admin&method=showAdmin">Admin</a></li>
				</p>

				<p class="footer-company-name">CTinformatique Name © 2019</p>
			</div>

			<div class="footer-center">

				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>61800</span> Chanu Beach, California</p>
				</div>

				<div>
					<i class="fa fa-phone"></i>
					<p>06 47 72 92 36 </p>
				</div>

				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:support@company.com">assistance.ctinformatique@gmail.com</a></p>
				</div>

			</div>

			<div class="footer-right">

				<p class="footer-company-about">
					<span>A propos</span>
					Nous sommes une équipe de passionnés dont l'objectif est d'améliorer la vie de chacun à l'aide de produits innovants. Nous construisons des produits remarquables pour résoudre les problèmes de votre entreprise.
					Nos articles sont conçus pour les petites et moyennes entreprises souhaitant optimiser leurs performances..
				</p>

			</div>

</footer>