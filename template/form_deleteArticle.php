<?php include 'layout/head_admin.php' ?>
<?php include 'layout/menu_admin.php' ?>

<h2>Supprimer un article</h2>

<form action="?url=admin&method=deleteArticle" method="post">
    <span>Voulez-vous supprimer l'article ?</span>

    <input type="hidden" name="id_article" value="<?=$id_article ?>">
    <input type="submit" class="btn btn-danger" value="Supprimer">
</form>

<?php include 'layout/footer_admin.php' ?>