<?php include 'layout/head.php' ?>

<form action="?url=admin&method=formLogin" method="post">
    <h2>Admin</h2>
    <div>
        <input type="text" name="username" id="identifiant" class="text-field" placeholder="Username" />
    </div>
    <div>
        <input type="password" name="password" id="password" class="text-field" placeholder="Password" />
    </div>
    <button name="btnConnection" type="submit">Connexion</button>
</form>

<?php include 'layout/footer.php' ?>
