En utilisant les principes de la POO, ainsi que les différents
éléments de cours. Vous devrez réaliser un système de gestion
d'article.
Vous aurez dans une base de données quelques articles d'enregistrés
et vous devrez réaliser le CRUD correspondant en PHP.

Une belle interface est un plus.

Pour vous aider :
N'hésitez pas à consulter les vidéos formations Grafikart sur PHP.
https://www.grafikart.fr/formations/php
https://www.grafikart.fr/formations/mysql
https://www.grafikart.fr/formations/programmation-objet-php

Ainsi que les différents tutoriaux présent sur OCR.
https://openclassrooms.com/fr/courses/1665806-programmez-en-oriente-objet-en-php