-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour master1820_article-poo
DROP DATABASE IF EXISTS `master1820_article-poo`;
CREATE DATABASE IF NOT EXISTS `master1820_article-poo` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `master1820_article-poo`;

-- Listage de la structure de la table master1820_article-poo. article
DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id_article` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(120) NOT NULL,
  `content` text NOT NULL,
  `img_path` varchar(255) NOT NULL,
  `author` varchar(30) NOT NULL,
  `created_date` date NOT NULL,
  `updated_date` date NOT NULL,
  PRIMARY KEY (`id_article`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Listage des données de la table master1820_article-poo.article : ~1 rows (environ)
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` (`id_article`, `title`, `content`, `img_path`, `author`, `created_date`, `updated_date`) VALUES
	(1, 'Mon premier article', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nisl tellus, congue sit amet convallis quis, tincidunt eu mauris. Aliquam sagittis tortor ac enim gravida, at aliquam ante tempus. Quisque semper erat nulla, sit amet dapibus ex molestie accumsan. Cras hendrerit eu odio sed viverra. Etiam enim est, porta a viverra nec, porttitor vel nibh. Proin nec mi nisi. In fermentum nisi nec massa pharetra, ut porta risus fermentum.', 'truc.png', 'Admin', '2019-05-21', '2019-05-21');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;

-- Listage de la structure de la table master1820_article-poo. user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(120) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Listage des données de la table master1820_article-poo.user : ~1 rows (environ)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id_user`, `username`, `password`) VALUES
	(1, 'admin', '$2y$10$ln4TTRqPpDMscv5Q9DM4cuJB6w70e6A4kWMbW98znUbh8YlSokJwm');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
