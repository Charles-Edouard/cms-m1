<?php
    header('Content-Type: application/json; charset=utf-8');
    $response = [];

    try {
        if (
            !isset($_FILES['upfile']['error']) || 
            is_array($_FILES['upfile']['error'])
            )
        {
            throw new RuntimeException('Paramètres invalides !');
        }

        switch ($_FILES['upfile']['error'])
        {
            case UPLOAD_ERR_OK:
                break;
            case UPLOAD_ERR_NO_FILE:
                throw new RuntimeException('Aucun fichier envoyé');
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                throw new RuntimeException('Taille de fichier trop importante');
            default:
                throw new RuntimeException('Erreur inconnu');
        }

        if ($_FILES['upfile']['size'] > 20 * 1024 * 1024) {
            throw new RuntimeException('Votre fichier ne doit pas être supérieur à 5Mo');
        }

        $fileInfo = new finfo(FILEINFO_MIME_TYPE);
        if (
            false === $ext = array_search(
                $fileInfo->file($_FILES['upfile']['tmp_name']),
                [
                    'jpg' => 'image/jpeg',
                    'jpeg' => 'image/jpeg',
                    'png' => 'image/png',
                    'gif' => 'image/gif'
                ],
                true
            )
        ) {
            throw new RuntimeException('Format du fichier invalide');
        }

        $fileName = sprintf(
            '%s.%s',
            sha1_file($_FILES['upfile']['tmp_name']),
            $ext
        );

        $destination = sprintf(
                            '../../public/images/%s',
                            $fileName
                       );

        if (!move_uploaded_file(
            $_FILES['upfile']['tmp_name'],
            $destination
        ))
        {
            throw new RuntimeException('Erreur dans le déplacement du fichier');
        }

        $response = [
            "status" => "success",
            "error" => false,
            "message" => "Fichier uploadé avec succés",
            "filename" => $fileName
        ];

        echo json_encode($response);
    } 
    catch(RuntimeException $e) {
        $response = [
            "status" => "error",
            "error" => true,
            "message" => $e->getMessage()
        ];

        echo json_encode($response);
    }
?>